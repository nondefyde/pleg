@extends('layouts.default')

@section('title', 'User Report Roles')

@section('breadcrumb')
    <li><a href="/report-roles">Report Roles</a></li>
    <li class="active">Manage</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-tasks"></span> User Report Roles</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12 center-block">

            </div>
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body panel-body-table">
                        {!! Form::open([
                                'method'=>'POST',
                                'class'=>'form',
                                'role'=>'form'
                            ])
                        !!}
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-actions" id="menu_table">
                                    <thead>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 15%;">Name</th>
                                            <th style="width: 7%;">Gender</th>
                                            <th style="width: 8%;">User Type</th>
                                            <th style="width: 45%;">Sector</th>
                                            {{--<th style="width: 15%;">Category</th>--}}
                                            <th style="width: 24%;">State</th>
                                        </tr>
                                    </thead>
                                    @if(count($users) > 0)
                                        <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($users as $user)
                                            <tr>
                                                <td class="text-center">{{$i++}} </td>
                                                <td>
                                                    <a href="{{ url('/users/show/'.$hashIds->encode($user->user_id)) }}" class="btn btn-link">{{ $user->fullNames() }}</a>
                                                    {!! Form::hidden('user_id[]', $user->user_id, ['class'=>'form-control']) !!}
                                                </td>
                                                <td>{!! $user->gender !!}</td>
                                                <td>{{ $user->userType()->first()->user_type }}</td>
                                                <td>
                                                    <select multiple class="form-control select" name="sector_id[{{$user->email}}][]" required>
                                                        @foreach($sectors as $sector)
                                                            @if(in_array($sector->sector_id, $user->reportSectors()->get()->lists('sector_id')->toArray()))
                                                                <option selected value="{{ $sector->sector_id }}">{{ $sector->sector }}</option>
                                                            @else
                                                                <option value="{{ $sector->sector_id }}">{{ $sector->sector }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
{{--                                                <td> {!! Form::select('category_id[]', [''=>'Federal', '1'=>'State'], ($user->reportStates()->count() > 0) ? 1 : 0, ['class'=>'form-control select', 'required'=>'required']) !!}</td>--}}
                                                <td>
                                                    <select multiple class="form-control select" name="state_id[{{$user->email}}][]">
                                                        @foreach($states as $state)
                                                            @if(in_array($state->state_id, $user->reportStates()->get()->lists('state_id')->toArray()))
                                                                <option selected value="{{ $state->state_id }}">{{ $state->state }}</option>
                                                            @else
                                                                <option value="{{ $state->state_id }}">{{ $state->state }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @endif
                                    <tfoot>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 15%;">Name</th>
                                            <th style="width: 7%;">Gender</th>
                                            <th style="width: 8%;">User Type</th>
                                            <th style="width: 45%;">Sector</th>
                                            {{--<th style="width: 15%;">Category</th>--}}
                                            <th style="width: 24%;">State</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-primary pull-right" type="submit">Save record</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection


@section('custom_script')
    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom/reports/roles.js') }}"></script>
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function() {
            setTabActive('[href="/report-roles"]');
        });
    </script>
@endsection