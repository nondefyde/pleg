@extends('layouts.default')

@section('title', 'Archived Reports')

@section('breadcrumb')
    <li class="active"><a href="/reports">Reports</a></li>
    <li class="active"><a href="/reports/archived">Archived</a></li>
    <li class="active">Manage</li>
@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-title">
        <h2><span class="fa fa-file-archive-o"></span> Archived Reports</h2>
    </div>

    <div class="page-content-wrap">

        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-12">
                <!-- START PANEL WITH REMOVE CALLBACKS -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <ul class="list-tags">
                                    @foreach($user->reportSectors()->get() as $sector)
                                        <li>
                                            <a href="{{ url('/reports/sectors/'.$hashIds->encode($sector->sector_id)) }}">
                                                <span class="fa fa-tag"> <span style="font-family: Tahoma"> {{ $sector->sector }}</span></span>
                                                ({{$user->getReportSectors($sector->sector_id)->count()}})
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-lg-4">
                                <form action="#" class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input id="quick-search" class="form-control" placeholder="Enter Keyword"/>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary ">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PANEL WITH REMOVE CALLBACKS -->
            </div>
        </div>
        <!-- END WIDGETS -->

        <!-- START RESPONSIVE TABLES -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Reports</h3>
                        <div id="loader" class="hide text-center">
                            <img class="img-circle" style="width:30px;" src="/images/loading.gif" alt="...">
                        </div>
                    </div>
                    <div class="panel-body panel-body-table">
                        <div class="table-responsive">
                            <table class="table table-bordered table-actions datatable">
                                <thead>
                                    <tr>
                                        <th style="width:1%;">#</th>
                                        <th style="width:5%;">Avatar</th>
                                        <th style="width:21%;">Name</th>
                                        <th style="width:40%;">Report Title</th>
                                        <th style="width:10%;">Sector</th>
                                        <th style="width:5%;">Backers</th>
                                        <th style="width:5%;">Summary</th>
                                        <th style="width:5%;">Detail</th>
                                        <th style="width:4%;">Action</th>
                                        <th style="width:4%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($reports) > 0)
                                        <?php $i = 1; ?>
                                        @foreach($reports as $report)
                                            <tr>
                                                <td class="text-center">{{ $i++ }}</td>
                                                <td>
                                                    <img src="/assets/images/users/user2.jpg" style="width:30px;" alt="John Doe" class="img-circle">
                                                </td>
                                                <td><strong><a href="#">{{ $report->mobileUser()->first()->fullNames() }}</a></strong></td>
                                                <td>{{ $report->title }}</td>
                                                <td>{{ $report->sector()->first()->sector }}</td>
                                                <td><span class="badge badge-primary">55</span></td>
                                                <td>
                                                    <button value="{{$report->report_id}}" class="btn btn-info btn-rounded btn-condensed btn-sm report_detail">
                                                        <span class="fa fa-info-circle"></span> View
                                                    </button>
                                                </td>
                                                <td>
                                                    <a href="{{ url('/reports/show/'.$hashIds->encode($report->report_id)) }}" class="btn btn-default btn-rounded btn-condensed btn-xs">
                                                        <span class="fa fa-eye-slash"></span>
                                                    </a>
                                                </td>
                                                <td>
                                                    <button  value="{{$report->report_id}}"class="btn btn-success btn-rounded btn-condensed btn-sm forward_report">
                                                        <span class="fa fa-send"></span> Forward
                                                    </button>
                                                </td>
                                                <td>
                                                    <button value="{{$report->report_id}}" rel="0" class="btn btn-warning btn-rounded btn-condensed btn-sm archive_report">
                                                        <span class="fa fa-file-archive-o"></span> UnArchive
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- END RESPONSIVE TABLES -->

    </div>

    <!-- MODALS -->
    <div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal_content">

            </div>
        </div>
    </div>
    <!-- END MODALS -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="confirm-forward-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span>You are about to <strong><span id="forward_name"></span></strong> Report?
                </div>
                <div class="mb-content">
                    <p>Press Yes to Continue.</p>
                    <p>Press No to cancel.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes" id="forward_status">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="message-box animated fadeIn" data-sound="alert" id="confirm-archive-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span>You are about to Un Archive <strong><span id="archive_name"></span></strong> Report?
                </div>
                <div class="mb-content">
                    <p>Press Yes to Continue.</p>
                    <p>Press No to cancel.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes" rel="" id="archive_status">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

@endsection

@section('custom_script')
    <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom/reports/report.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/reports/archived"]');
        });
    </script>
@endsection