@extends('layouts.default')

@section('title', 'Add Report')

@section('breadcrumb')
    <li class="active"><a href="/reports">Reports</a></li>
    <li class="active">Create</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('errors.errors')
            <form method="POST" action="/reports/create" enctype="multipart/form-data" accept-charset="UTF-8" class="form-horizontal" role="form">
                {!! csrf_field() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Add New Report</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Report Title</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" placeholder="Report Title" name="title" value="{{ Input::old('title') }}" class="form-control"/>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">
                                <textarea class="form-control" placeholder="Report Description"  name="description" rows="5">{{ Input::old('description') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Report Time</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input type="text" placeholder="Report Time" name="report_time" value="{{ Input::old('report_time') }}" class="form-control datepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Report Sector</label>
                            <div class="col-md-6 col-xs-12">
                                {!! Form::select('sector_id', $sectors, Input::old('sector_id'), ['class'=>'form-control select', 'id'=>'sector_id']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Reported By</label>
                            <div class="col-md-6 col-xs-12">
                                <select name="mobile_user_id" class="select form-control">
                                    <option value="">Nothing Selected</option>
                                    @foreach($mobile_users as $mobile_user)
                                        <option value="{{$mobile_user->mobile_user_id}}">{{$mobile_user->fullNames()}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Report State</label>
                            <div class="col-md-6 col-xs-12">
                                {!! Form::select('state_id', $states, Input::old('state_id'), ['class'=>'form-control select', 'id'=>'state_id']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Report Image</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="file" class="fileinput btn-primary"name="image_url" title="Browse Image (jpg, png)" data-filename-placement="inside"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Location</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="location_id" class="form-control"/>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-default">Clear</button>
                        <button class="btn btn-primary pull-right">Submit Report</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('custom_script')
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <script type='text/javascript' src='{{ asset('/js/plugins/bootstrap/bootstrap-datepicker.js') }}'></script>

    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/reports/create"]');
        });
    </script>
@endsection