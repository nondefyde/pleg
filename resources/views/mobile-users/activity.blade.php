@extends('layouts.default')

@section('title', 'Mobile Users Activities')

@section('breadcrumb')
    <li class="active"><a href="/reports">User Activities</a></li>
    <li class="active">View Details</li>
@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-title">
        <h2><span class="fa fa-clock-o"></span> Report TimeLine on User Activities</h2>
    </div>
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <!-- START PANEL WITH REMOVE CALLBACKS -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <img src="{{($mobile_user->avatar) ? $mobile_user->avatar : '/assets/images/users/no-image.jpg'}}" style="width:30px;" alt="{{$mobile_user->first_name}}"
                                     class="img-circle">
                                <span class="label label-primary">{{$mobile_user->fullNames()}}</span>
                                <span class="label label-primary">{{$mobile_user->email}}</span>
                                <span class="label label-primary">{{$mobile_user->mobile}}</span>
                                <span class="label label-primary">{{$mobile_user->gender}}</span>
                                {!! ($mobile_user->lga_id) ? '<span class="label label-primary">' . $mobile_user->lga()->first()->state()->first()->state . '</span>' : '' !!}
                                {!! ($mobile_user->lga_id) ? '<span class="label label-primary">' . $mobile_user->lga()->first()->lga . '</span>' : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PANEL WITH REMOVE CALLBACKS -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- START TIMELINE -->
                    <div class="timeline timeline-right">

                        <!-- START TIMELINE ITEM -->
                        <div class="timeline-item timeline-main">
                            <div class="timeline-date">2015</div>
                        </div>
                        <!-- END TIMELINE ITEM -->

                        <!-- START TIMELINE ITEM -->
                        <div class="timeline-item timeline-item-right">
                            <div class="timeline-item-info">5 Sep 2015</div>
                            <div class="timeline-item-icon"><span class="fa fa-book"></span></div>
                            <div class="timeline-item-content">
                                @if($mobile_user->reports()->count() > 0)
                                    @foreach($mobile_user->reports()->orderBy('created_at', 'desc')->get() as $report)
                                        <div class="timeline-heading">
                                            <img src="{{($mobile_user->avatar) ? $mobile_user->avatar : '/assets/images/users/no-image.jpg'}}" style="width:30px;" class="img-circle"/>
                                            <a href="#">{{$mobile_user->fullNames()}}</a> reported
                                            <a href="{{ url('/reports/show/'.$hashIds->encode($report->report_id)) }}">{{$report->title}}</a>
                                            <small class="text-muted">{{$report->created_at->diffForHumans()}}</small>
                                            {!! ($report->approve === 1) ? '<label class="label label-success">Approved</label>' : '<label class="label label-warning">Not Approve</label>' !!}
                                            {!! ($report->archive === 1) ? '<label class="label label-danger">Archived</label>' : '' !!}
                                        </div>
                                        <div class="timeline-body" id="links">
                                            @if($report->images())
                                                <div class="gallery" id="links">
                                                    @for($i = 0; $i < count($report->images()); $i++)
                                                        <a class="gallery-item" href="{{asset($report->file_path.$report->images()[$i])}}" title="{{$report->title}}" data-gallery>
                                                            <div class="image">
                                                                <img src="{{asset($report->file_path.$report->images()[$i])}}" alt="..."/>
                                                            </div>
                                                        </a>
                                                    @endfor
                                                </div>
                                            @endif
                                            {{--@if($report->images()->count() > 0)--}}
                                                {{--<div class="row">--}}
                                                    {{--@foreach($report->images()->get() as $image)--}}
                                                        {{--<div class="col-md-1">--}}
                                                            {{--<a href="{{($image->image_url) ? $image->file_path . $image->image_url : '/assets/images/users/no-image.jpg'}}" title="Image" data-gallery>--}}
                                                                {{--<img src="{{($image->image_url) ? $image->file_path . $image->image_url : '/assets/images/users/no-image.jpg'}}" class="img-responsive img-text"/>--}}
                                                            {{--</a>--}}
                                                        {{--</div>--}}
                                                    {{--@endforeach--}}
                                                {{--</div>--}}
                                            {{--@endif--}}
                                            <div class="col-lg-12">
                                                <p>{{($report->description) ? $report->description : ''}}</p>
                                                <a href="{{ url('/backers') }}" class="btn btn-primary btn-sm"> Backers
                                                    <span class="badge">55</span>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <!-- END TIMELINE ITEM -->

                        <!-- START TIMELINE ITEM -->
                        <div class="timeline-item timeline-main">
                            <div class="timeline-date"><a href="#"><span class="fa fa-ellipsis-h"></span></a></div>
                        </div>
                        <!-- END TIMELINE ITEM -->
                    </div>
                    <!-- END TIMELINE -->

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('custom_script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/reports"]');
        });
    </script>
@endsection