@extends('layouts.default')

@section('title', 'Error 401')

@section('breadcrumb')
    <li><a href="#">Error</a></li>
    <li class="active">Error: 401</li>
@endsection

@section('content')
    <div class="page-title">
        <h2><span class="fa fa-lock"></span> Error 401: Access Denial</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">

                <div class="error-container">
                    <div class="error-code">401</div>
                    <div class="error-text">Access Denial Error</div>
                    <div class="error-subtext">Whoops!!! You have no access / privilege to perform such action / operation.</div>

                </div>

            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->

@endsection

@section('custom_script')
    <script>
        jQuery(document).ready(function() {
            setTabActive('[href="/dashboard"]');
        });
    </script>
@endsection