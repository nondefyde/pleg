@extends('layouts.default')

@section('title', 'Password Change')

@section('breadcrumb')
    <li><a href="/auth/change">Password</a></li>
    <li class="active">Change</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-lock"></span> Password Change</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                @include('errors.errors')
                <form method="POST" action="/users/change" accept-charset="UTF-8" class="form-horizontal"
                      role="form">
                    {!! csrf_field() !!}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><span class="fa fa-pencil"></span> New User</h3>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">Old Password</label>

                                <div class="col-md-9 col-xs-7">
                                    <input name="password" type="password" min="6" class="form-control"
                                           placeholder="Old Password" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">New Password</label>

                                <div class="col-md-9 col-xs-7">
                                    <input name="new_password" type="password" min="6" class="form-control"
                                           placeholder="New Password" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">Confirm Password</label>

                                <div class="col-md-9 col-xs-7">
                                    <input name="password_confirmation" min="6" type="password" class="form-control"
                                           placeholder="Confirm Password" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-xs-5">
                                    <input class="btn btn-primary btn-rounded pull-right" type="submit"
                                           value="Change">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

@endsection

@section('custom_script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/users/change"]');
        });
    </script>
@endsection