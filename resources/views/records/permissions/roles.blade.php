@extends('layouts.default')

@section('title', 'Permissions To Roles')

@section('breadcrumb')
    <li><a href="/permissions/edit-permission"> Permissions To Roles</a></li>
    <li class="active">Manage</li>
@endsection

@section('content')
    <div class="page-title">
        <h2><span class="fa fa-sitemap"></span> Permissions To Roles</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Filter By Roles</h3>
                        <form method="post" action="/permissions/roles" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-md-3 control-label">Roles</label>

                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <select class="form-control select" name="role_id" id="role_id">
                                            @foreach($roles as $key => $value)
                                                @if($key === $role->role_id)
                                                    <option selected value="{{$key}}">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary pull-right" type="submit">Filter</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <h3 class="text-center">Permissions in: <span class="text-danger">{{ $role->display_name }}</span> Role</h3>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <form method="post" action="/permissions/roles-permissions" role="form" class="form-horizontal">
                        <div class="panel-heading">
                            <h3 class="panel-title">List of Permissions</h3>
                        </div>
                        <div class="panel-heading">
                            <label style="font-size: small" class="pull-left"><input type="checkbox" class="permissions_all" value="0" name=""/>
                                <span>Add All</span>
                            </label>
                            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i>Save record</button>
                        </div>
                        <div class="panel-body panel-body-table">
                            {!! csrf_field() !!}
                            {!! Form::hidden('role_id', $role->role_id, ['class'=>'form-control']) !!}

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="panel-body">
                                        <ul class="list-group border-bottom">
                                            @for($i=0; $i<count($permissions); $i+=3)
                                                <li class="list-group-item"><small>({{$i+1}}) {{$permissions[$i]->name}}</small>
                                                    <div class="list-group-controls">
                                                        <div class="col-md-12">
                                                            <label>
                                                                @if( in_array($permissions[$i]->permission_id, array_values($role->perms()->get()->lists('permission_id')->toArray())))
                                                                    <input checked type="checkbox" name="permission_id[]" value="{{ $permissions[$i]->permission_id }}" class="permissions_check_box color_border"/>
                                                                    <span class="label label-danger">Remove</span>
                                                                @else
                                                                    <input type="checkbox" name="permission_id[]" value="{{ $permissions[$i]->permission_id }}" class="permissions_check_box"/>
                                                                    <span class="label label-success">Add</span>
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="panel-body col-md-4">
                                        <ul class="list-group border-bottom">
                                            @for($j=1; $j<count($permissions); $j+=3)
                                                <li class="list-group-item"><small>({{$j+1}}) {{$permissions[$j]->name}}</small>
                                                    <div class="list-group-controls">
                                                        <div class="col-md-12">
                                                            <label>
                                                                @if( in_array($permissions[$j]->permission_id, array_values($role->perms()->get()->lists('permission_id')->toArray())))
                                                                    <input checked type="checkbox" name="permission_id[]" value="{{ $permissions[$j]->permission_id }}" class="permissions_check_box color_border"/>
                                                                    <span class="label label-danger">Remove</span>
                                                                @else
                                                                    <input type="checkbox" name="permission_id[]" value="{{ $permissions[$j]->permission_id }}" class="permissions_check_box"/>
                                                                    <span class="label label-success">Add</span>
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel-body col-md-4">
                                        <ul class="list-group border-bottom">
                                            @for($k=2; $k<count($permissions); $k+=3)
                                                <li class="list-group-item"><small>({{$k+1}}) {{$permissions[$k]->name}}</small>
                                                    <div class="list-group-controls">
                                                        <div class="col-md-12">
                                                            <label>
                                                                @if( in_array($permissions[$k]->permission_id, array_values($role->perms()->get()->lists('permission_id')->toArray())))
                                                                    <input checked type="checkbox" name="permission_id[]" value="{{ $permissions[$k]->permission_id }}" class="permissions_check_box color_border"/>
                                                                    <span class="label label-danger">Remove</span>
                                                                @else
                                                                    <input type="checkbox" name="permission_id[]" value="{{ $permissions[$k]->permission_id }}" class="permissions_check_box"/>
                                                                    <span class="label label-success">Add</span>
                                                                @endif
                                                            </label>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <label style="font-size: small" class="pull-left"><input type="checkbox" class="permissions_all" value="0" name=""/>
                                <span>Add All</span>
                            </label>
                            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i>Save record</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('custom_script')
    <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/custom/records/permissions.js') }}"></script>
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/permissions/roles-permissions"]');
        });
    </script>
@endsection