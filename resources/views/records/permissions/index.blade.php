@extends('layouts.default')

@section('title', 'Permissions')

@section('breadcrumb')
    <li><a href="/permissions">Permissions</a></li>
    <li class="active">Manage</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-location-arrow"></span> Permissions</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">List of Permissions</h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="/permissions" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-actions">
                                    <thead>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 19%;">Controller</th>
                                            <th style="width: 10%;">Action</th>
                                            <th style="width: 10%;">URI</th>
                                            <th style="width: 20%;">Display Name</th>
                                            <th style="width: 40%;">Description</th>
                                        </tr>
                                    </thead>
                                    @if(count($controllers) > 0)
                                        <tbody>
                                        <?php $i = 0; ?>
                                        @foreach($controllers as $controller)
                                            {{--{{dd($controller)}}--}}
                                            @if(isset($permissions[$i]) and trim($controller->name) === trim($permissions[$i]->name))
                                                <tr>
                                                    <td class="text-center">{{$i + 1}}</td>
                                                    <td>{{ explode('@', $controller->name)[0]}}</td>
                                                    <td>
                                                        {{ explode('@', $controller->name)[1]}}
                                                        {!! Form::hidden('name[]', $controller->name, ['class'=>'form-control']) !!}
                                                        {!! Form::hidden('permission_id[]', $permissions[$i]->permission_id, ['class'=>'form-control']) !!}
                                                    </td>
                                                    <td>
                                                        {{ $controller->uri }}
                                                        {!! Form::hidden('uri[]', $controller->uri, ['class'=>'form-control']) !!}
                                                    </td>
                                                    <td>{!! Form::text('display_name[]', $permissions[$i]->display_name  , ['placeholder'=>'Permission Display Name', 'class'=>'form-control']) !!} </td>
                                                    <td>{!! Form::text('description[]', $permissions[$i]->description  , ['placeholder'=>'Permission Description', 'class'=>'form-control']) !!} </td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="text-center">{{$i + 1}}</td>
                                                    <td>{{ explode('@', $controller->name)[0]}}</td>
                                                    <td>
                                                        {{ explode('@', $controller->name)[1]}}
                                                        {!! Form::hidden('name[]', $controller->name, ['class'=>'form-control']) !!}
                                                        {!! Form::hidden('permission_id[]', ($i+1), ['class'=>'form-control']) !!}
                                                    </td>
                                                    <td>
                                                        {{ $controller->uri }}
                                                        {!! Form::hidden('uri[]', $controller->uri, ['class'=>'form-control']) !!}
                                                    </td>
                                                    <td>{!! Form::text('display_name[]', '' , ['placeholder'=>'Permission Display Name', 'class'=>'form-control']) !!} </td>
                                                    <td>{!! Form::text('description[]', '', ['placeholder'=>'Permission Description', 'class'=>'form-control']) !!} </td>
                                                </tr>
                                            @endif
                                            <?php $i++; ?>
                                        @endforeach
                                        </tbody>
                                    @endif
                                    <tfoot>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 19%;">Controller</th>
                                            <th style="width: 10%;">Action</th>
                                            <th style="width: 10%;">URI</th>
                                            <th style="width: 20%;">Display Name</th>
                                            <th style="width: 40%;">Description</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-primary pull-right" type="submit">Save record</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    @endsection

    @section('custom_script')
        <script type="text/javascript" src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/permissions"]');
        });
    </script>
@endsection