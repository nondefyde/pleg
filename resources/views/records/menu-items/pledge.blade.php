@extends('layouts.default')

@section('title', 'Pledge Menu Items')

@section('breadcrumb')
    <li><a href="/pledge-menu-items">Pledge Menu Items</a></li>
    <li class="active">Manage</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-sitemap"></span> Pledge Menu Items</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Filter By Pledge Menu</h3>

                        <form method="post" action="/pledge-menu-items/menu" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-md-3 control-label">Menu</label>

                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <select class="form-control select" name="pledge_menu_id" id="menu_select">
                                            <option value="">Display All Menus</option>
                                            @foreach($pledge_menu_lists as $key => $value)
                                                @if($pledge_menu_id === $key)
                                                    <option selected value="{{$key}}">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary pull-right" type="submit">Filter</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-default add_menu"><i class="fa fa-plus"></i> Add</button>
                    </div>
                    <div class="panel-body panel-body-table">
                        <form method="post" action="/pledge-menu-items" id="menu_item_form" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-actions" id="menu_table">
                                    <thead>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 16%;">Menu item</th>
                                            <th style="width: 15%;">Menu item url</th>
                                            <th style="width: 12%;">Menu item icon</th>
                                            <th style="width: 14%;">Parent menu</th>
                                            <th style="width: 10%;">Status</th>
                                            <th style="width: 21%;">Role</th>
                                            <th style="width: 6%;">Order</th>
                                            <th style="width: 5%;">actions</th>
                                        </tr>
                                    </thead>
                                    @if(count($pledge_menu_items) > 0)
                                        <tbody id="menu_item_tbody">
                                        <?php $i = 1; ?>
                                        @foreach($pledge_menu_items as $menu_item)
                                            <tr>
                                                <td class="text-center">{{$i++}}</td>
                                                <td>
                                                    {!! Form::text('pledge_menu_item[]', $menu_item->pledge_menu_item, ['placeholder'=>'Menu Item', 'class'=>'form-control', 'required'=>'required']) !!}
                                                    {!! Form::hidden('pledge_menu_item_id[]', $menu_item->pledge_menu_item_id, ['class'=>'form-control']) !!}
                                                </td>
                                                <td>{!! Form::text('pledge_menu_item_url[]', $menu_item->pledge_menu_item_url, ['placeholder'=>'Menu Item Url', 'class'=>'form-control', 'required'=>'required']) !!}</td>
                                                <td>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="{{$menu_item->pledge_menu_item_icon}}"></i></span>
                                                        {!! Form::text('pledge_menu_item_icon[]', $menu_item->pledge_menu_item_icon, ['placeholder'=>'Menu Icon', 'class'=>'form-control', 'required'=>'required']) !!}
                                                    </div>
                                                </td>
                                                <td>{!! Form::select('pledge_menu_id[]', $pledge_menu_lists, $menu_item->pledge_menu_id, ['class'=>'form-control']) !!} </td>
                                                <td> {!! Form::select('active[]', [''=>'Status', 1=>'Enable', 0=>'Disable'], $menu_item->active, ['class'=>'form-control', 'required'=>'required']) !!}</td>
                                                <td>
                                                    <select multiple class="form-control select" name="role_id[{{$menu_item->pledge_menu_item}}][]">
                                                        @foreach($roles as $role)
                                                            @if(in_array($role->role_id, $menu_item->pledgeRoles()->get()->lists('role_id')->toArray()))
                                                                <option selected value="{{ $role->role_id }}">{{ $role->display_name }}</option>
                                                            @else
                                                                <option value="{{ $role->role_id }}">{{ $role->display_name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>{!! Form::text('sequence[]', $menu_item->sequence, ['placeholder'=>'Order By', 'class'=>'form-control', 'required'=>'required']) !!}</td>
                                                <td>
                                                    <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_menu">
                                                        <span class="fa fa-trash-o"></span> Delete
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @else
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td>
                                                {!! Form::text('pledge_menu_item[]', '', ['placeholder'=>'Menu Item', 'class'=>'form-control', 'required'=>'required']) !!}
                                                {!! Form::hidden('pledge_menu_item_id[]', '-1', ['class'=>'form-control']) !!}
                                            </td>
                                            <td>{!! Form::text('pledge_menu_item_url[]', '', ['placeholder'=>'Menu Item Url', 'class'=>'form-control', 'required'=>'required']) !!}</td>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class=""></i></span>
                                                    {!! Form::text('pledge_menu_item_icon[]', '', ['placeholder'=>'Menu Item Icon', 'class'=>'form-control', 'required'=>'required']) !!}
                                                </div>
                                            </td>
                                            <td>{!! Form::select('pledge_menu_id[]', $pledge_menu_lists, '', ['class'=>'form-control', 'required'=>'required']) !!} </td>
                                            <td>{!! Form::select('active[]', [''=>'Status', 1=>'Enable', 0=>'Disable'],'', ['class'=>'form-control', 'required'=>'required']) !!}</td>
                                            <td>
                                                <select multiple class="form-control select" name="role_id[][]">
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role->role_id }}">{{ $role->display_name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>{!! Form::text('sequence[]', '', ['placeholder'=>'Order By', 'class'=>'form-control', 'required'=>'required']) !!}</td>
                                            <td>
                                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm">
                                                    <span class="fa fa-times"></span> Remove
                                                </button>
                                            </td>
                                        </tr>
                                    @endif
                                    <tfoot>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 16%;">Menu item</th>
                                            <th style="width: 15%;">Menu item url</th>
                                            <th style="width: 12%;">Menu item icon</th>
                                            <th style="width: 14%;">Parent menu</th>
                                            <th style="width: 10%;">Status</th>
                                            <th style="width: 21%;">Role</th>
                                            <th style="width: 6%;">Order</th>
                                            <th style="width: 5%;">actions</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-default pull-left add_menu"><i class="fa fa-plus"></i> Add</button>
                                <button class="btn btn-primary pull-right" type="submit">Save record</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="confirm-remove-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span> Remove <strong id="menu_value"></strong> ?
                </div>
                <div class="mb-content">
                    <p>Are you sure you want to remove this pledge menu item?</p>

                    <p>Press Yes if you sure.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes" id="confirm_menu_item_delete">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    @endsection


    @section('custom_script')
            <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/custom/records/menu_item.js') }}"></script>
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/pledge-menu-items"]');
        });
    </script>
@endsection