@extends('layouts.default')

@section('title', 'Manage User Roles')

@section('breadcrumb')
    <li><a href="/roles">Roles</a></li>
    <li class="active">Manage Roles</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-lock"></span> Manage User Roles</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Filter By Roles</h3>
                        <form method="post" action="/roles/roles" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-md-3 control-label">Roles</label>

                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <select class="form-control select" name="role_id" id="role_id">
                                            @foreach($roles as $key => $value)
                                                @if($key === $role->role_id)
                                                    <option selected value="{{$key}}">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary pull-right" type="submit">Enter</button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10">
                                    <h3 class="text-center">Users in: <span class="text-danger">{{ $role->display_name }}</span> Role</h3>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <!-- START DEFAULT DATATABLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">List of Users and Roles</h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="/roles/users-roles/{{$encodeId}}" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-actions datatable">
                                    <thead>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 19%;">Names</th>
                                            <th style="width: 19%;">Email</th>
                                            <th style="width: 5%;">Gender</th>
                                            <th style="width: 13%;">User Type</th>
                                            <th style="width: 33%;">Roles</th>
                                            <th style="width: 5%;">View</th>
                                            <th style="width: 5%;">Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($users) > 0)
                                        <?php $i = 1; ?>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$i++}} </td>
                                                <td>{{ $user->simpleName() }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{!! ($user->gender) ? $user->gender : '<span class="label label-danger">nil</span>' !!}</td>
                                                <td>{{ $user->userType()->first()->user_type }}</td>
                                                <td>
                                                    <select multiple class="form-control select" name="role_id[role{{$user->user_id}}][]">
                                                        @foreach($roles as $key => $value)
                                                            @if( in_array($key, $user->roles()->get()->lists('role_id')->toArray()))
                                                                <option selected value="{{$key}}">{{$value}}</option>
                                                            @else
                                                                <option value="{{$key}}">{{$value}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <input name="user_id[]" type="hidden" value="{{$user->user_id}}">
                                                </td>
                                                <td>
                                                    <a target="_blank" href="{{ url('/users/show/'.$hashIds->encode($user->user_id)) }}" class="btn btn-info btn-rounded btn-condensed btn-xs">
                                                        <span class="fa fa-eye-slash"></span>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ url('/users/edit/'.$hashIds->encode($user->user_id)) }}" class="btn btn-warning btn-rounded btn-condensed btn-xs">
                                                        <span class="fa fa-edit"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 19%;">Names</th>
                                            <th style="width: 19%;">Email</th>
                                            <th style="width: 5%;">Gender</th>
                                            <th style="width: 13%;">User Type</th>
                                            <th style="width: 33%;">Roles</th>
                                            <th style="width: 5%;">View</th>
                                            <th style="width: 5%;">Edit</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-default pull-left add_menu"><i class="fa fa-plus"></i> Add</button>
                                <button class="btn btn-primary pull-right" type="submit">Save record</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END DEFAULT DATATABLE -->
            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('custom_script')
    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
{{--    <script type="text/javascript" src="{{ asset('/js/custom/roles.js') }}"></script>--}}
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function() {
            setTabActive('[href="/roles/users-roles"]');
        });
    </script>
@endsection