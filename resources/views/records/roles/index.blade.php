@extends('layouts.default')

@section('title', 'Roles')

@section('breadcrumb')
    <li><a href="/roles">Roles</a></li>
    <li class="active">Manage</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-sitemap"></span> Roles</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-default add_role"><i class="fa fa-plus"></i> Add</button>
                    </div>
                    <div class="panel-body panel-body-table">
                        <form method="post" action="/roles" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-actions" id="role_table">
                                <thead>
                                <tr>
                                    <th style="width: 1%;">#</th>
                                    <th style="width: 15%;">Role (Unique)</th>
                                    <th style="width: 22%;">Display Name</th>
                                    <th style="width: 35%;">Description</th>
                                    <th style="width: 22%;">User Type (Unique)</th>
                                    <th style="width: 5%;">Actions</th>
                                </tr>
                                </thead>
                                @if(count($roles) > 0)
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($roles as $role)
                                        <tr>
                                            <td class="text-center">{{$i++}}</td>
                                            <td>
                                                {!! Form::text('name[]', $role->name, ['placeholder'=>'Role Name (Unique)', 'class'=>'form-control', 'required'=>'required']) !!}
                                                {!! Form::hidden('role_id[]', $role->role_id, ['class'=>'form-control']) !!}
                                            </td>
                                            <td>{!! Form::select('display_name[]', $user_type_list, $role->display_name, ['class'=>'form-control', 'required'=>'required']) !!} </td>
                                            <td>{!! Form::text('description[]', $role->description  , ['placeholder'=>'Role Description', 'class'=>'form-control']) !!} </td>
                                            <td>{!! Form::select('user_type_id[]', $user_types, $role->user_type_id, ['class'=>'form-control', 'required'=>'required']) !!} </td>
                                            <td>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @else
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>
                                            {!! Form::text('name[]', '', ['placeholder'=>'Role Name (Unique)', 'class'=>'form-control', 'required'=>'required']) !!}
                                            {!! Form::hidden('role_id[]', '-1', ['class'=>'form-control']) !!}
                                        </td>
                                        <td>{!! Form::select('display_name[]', $user_type_list, '', ['class'=>'form-control', 'required'=>'required']) !!} </td>
                                        <td>{!! Form::text('description[]', ''  , ['placeholder'=>'Role Description', 'class'=>'form-control']) !!} </td>
                                        <td>{!! Form::select('user_type_id[]', $user_types, '', ['class'=>'form-control', 'required'=>'required']) !!} </td>
                                        <td>
                                            <button class="btn btn-danger btn-rounded btn-condensed btn-sm">
                                                <span class="fa fa-times"></span> Remove
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                                <tfoot>
                                <tr>
                                    <th style="width: 1%;">#</th>
                                    <th style="width: 15%;">Role (Unique)</th>
                                    <th style="width: 22%;">Display Name</th>
                                    <th style="width: 35%;">Description</th>
                                    <th style="width: 22%;">User Type (Unique)</th>
                                    <th style="width: 5%;">Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <button type="button" class="btn btn-default pull-left add_role"><i class="fa fa-plus"></i> Add</button>
                            <button class="btn btn-primary pull-right" type="submit">Save record</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="confirm-remove-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span> Remove <strong id="role_value"></strong> Role?
                </div>
                <div class="mb-content">
                    <p>Are you sure you want to remove this role?</p>

                    <p>Press Yes if you sure.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes" id="confirm_role_delete">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    @endsection


    @section('custom_script')
            <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/custom/records/roles.js') }}"></script>
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/roles"]');
        });
    </script>
@endsection