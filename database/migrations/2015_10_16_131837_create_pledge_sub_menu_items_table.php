<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePledgeSubMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pledge_sub_menu_items', function (Blueprint $table) {
            $table->increments('pledge_sub_menu_item_id');
            $table->string('pledge_sub_menu_item', 150);
            $table->string('pledge_sub_menu_item_url', 150);
            $table->string('pledge_sub_menu_item_icon', 100);
            $table->integer('active')->unsigned()->default(1);
            $table->string('sequence');
            $table->integer('pledge_menu_item_id')->unsigned()->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pledge_sub_menu_items');
    }
}
