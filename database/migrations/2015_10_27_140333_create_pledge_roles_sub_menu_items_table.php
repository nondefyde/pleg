<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePledgeRolesSubMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pledge_roles_sub_menu_items', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('role_id')->on('roles')->onDelete('cascade');

            $table->integer('pledge_sub_menu_item_id')->nullable()->unsigned()->index();
            $table->foreign('pledge_sub_menu_item_id')->references('pledge_sub_menu_item_id')->on('pledge_sub_menu_items')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pledge_roles_sub_menu_items');
    }
}
