<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisation_heads', function (Blueprint $table) {
            $table->increments('organisation_head_id');
            $table->integer('parent_user_id')->unsigned()->index();
            $table->integer('child_user_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('parent_user_id')->references('user_id')
                ->on('users')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organisation_heads');
    }
}
