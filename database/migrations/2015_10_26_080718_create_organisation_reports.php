<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisation_reports', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('organisation_id')->unsigned()->index();
            $table->foreign('organisation_id')->references('organisation_id')->on('organisations')->onDelete('cascade');

            $table->integer('report_id')->unsigned()->index();
            $table->foreign('report_id')->references('report_id')->on('reports')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organisation_reports');
    }
}
