<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('report_id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->timestamp('report_time');
            $table->integer('forward')->default(0);
            $table->integer('archive')->default(0);
            $table->integer('spam')->default(0);
            $table->integer('notification')->default(0);
            $table->text('images')->nullable();
            $table->integer('sector_id')->unsigned()->index();
            $table->integer('mobile_user_id')->unsigned()->index();
            $table->integer('lga_id')->nullable()->unsigned()->index();
            $table->integer('state_id')->nullable()->unsigned()->index();
            $table->string('gps')->nullable()->index();
            $table->text('address')->nullable()->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
