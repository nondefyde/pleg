<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();
            $table->date('dob')->nullable();
            $table->string('email')->unique();
            $table->string('gender');
            $table->string('mobile')->nullable();
            $table->integer('salutation_id')->nullable()->unsigned()->index();
            $table->integer('lga_id')->unsigned()->index();
            $table->integer('user_type_id')->unsigned()->index();
            $table->string('avatar')->nullable();
            $table->integer('status')->default(1);
            $table->integer('verified')->default(0);
            $table->string('verification_code')->nullable();
            $table->string('password', 150);
            $table->rememberToken();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}