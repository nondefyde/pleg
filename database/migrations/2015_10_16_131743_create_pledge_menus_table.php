<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePledgeMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pledge_menus', function (Blueprint $table) {
            $table->increments('pledge_menu_id');
            $table->string('pledge_menu', 150);
            $table->string('pledge_menu_url', 150)->nullable();
            $table->string('pledge_icon');
            $table->integer('active')->unsigned()->default(1);
            $table->integer('sequence')->unsigned();
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pledge_menus');
    }
}
