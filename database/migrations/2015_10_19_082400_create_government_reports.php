<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGovernmentReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('government_reports', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('government_report_id');
            $table->integer('report_id')->unsigned()->index();
            $table->integer('sector_status')->unsigned()->index()->default(0);
            $table->integer('legislator_status')->unsigned()->index()->default(0);
            $table->integer('sector_notification')->unsigned()->index()->default(0);
            $table->integer('executive_notification')->unsigned()->index()->default(0);
            $table->integer('legislator_notification')->unsigned()->index()->default(0);
            $table->timestamps();

            $table->foreign('report_id')->references('report_id')->on('reports')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('government_reports');
    }
}
