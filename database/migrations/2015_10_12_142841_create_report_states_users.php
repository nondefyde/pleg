<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportStatesUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_states_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('state_id')->unsigned()->index();
            $table->foreign('state_id')->references('state_id')->on('states')->onDelete('cascade');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');

//            $table->integer('category_id')->unsigned()->index()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_states_users');
    }
}
