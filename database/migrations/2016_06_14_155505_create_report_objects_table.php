<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_comments', function (Blueprint $table) {
            $table->increments('report_comment_id');
            $table->text('comment_body')->nullable();
            $table->integer('mobile_user_id')->unsigned()->index();
            $table->integer('report_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::create('report_followers', function (Blueprint $table) {
            $table->increments('report_follower_id');
            $table->integer('mobile_user_id')->unsigned()->index();
            $table->integer('report_id')->unsigned()->index();
            $table->integer('followed')->default(0)->index();
            $table->timestamps();
        });

        Schema::create('report_votes', function (Blueprint $table) {
            $table->increments('report_vote_id');
            $table->integer('mobile_user_id')->unsigned()->index();
            $table->integer('report_id')->unsigned()->index();
            $table->integer('voted')->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_comments');
        Schema::drop('report_followers');
        Schema::drop('report_votes');
    }
}
