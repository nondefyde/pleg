<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_updates', function (Blueprint $table) {
            $table->increments('report_update_id');
            $table->text('update_body')->nullable();
            $table->integer('mobile_user_id')->unsigned()->index();
            $table->integer('report_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_updates');
    }
}
