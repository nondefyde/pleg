<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisations', function (Blueprint $table) {
            $table->increments('organisation_id');
            $table->string('name');
            $table->string('cac_registration_no')->nullable();
            $table->string('address');
            $table->string('email')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('description')->nullable();
            $table->string('logo_url')->nullable();
            $table->string('responsibility')->nullable();
            $table->integer('sector_id')->unsigned()->index();
            $table->integer('lga_id')->unsigned()->index();
            $table->integer('head_user_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('sector_id')->references('sector_id')
                ->on('sectors')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organisations');
    }
}
