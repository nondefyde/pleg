<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesSubMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_sub_menu_items', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('role_id')->on('roles')->onDelete('cascade');

            $table->integer('sub_menu_item_id')->nullable()->unsigned()->index();
            $table->foreign('sub_menu_item_id')->references('sub_menu_item_id')->on('sub_menu_items')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles_sub_menu_items');
    }
}
