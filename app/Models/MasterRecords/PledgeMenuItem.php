<?php

namespace App\Models\MasterRecords;

use Illuminate\Database\Eloquent\Model;

class PledgeMenuItem extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pledge_menu_items';

    /**
     * The table Ranks primary key
     *
     * @var int
     */
    protected $primaryKey = 'pledge_menu_item_id';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['pledge_menu_item', 'pledge_menu_item_url', 'pledge_menu_item_icon', 'active', 'sequence', 'menu_id'];

    /**
     * A Menu Item belongs to a Menu
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pledgeMenu(){
        return $this->belongsTo('App\Models\MasterRecords\PledgeMenu');
    }

    /**
     * A Menu item has many sub Menu Items
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pledgeSubMenuItems(){
        return $this->hasMany('App\Models\MasterRecords\PledgeSubMenuItem');
    }

    /**
     * Get the roles associated with the given menu item
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pledgeRoles()
    {
        return $this->belongsToMany('App\Models\RolesAndPermissions\Role', 'pledge_roles_menu_items', 'pledge_menu_item_id', 'role_id');
    }
}
