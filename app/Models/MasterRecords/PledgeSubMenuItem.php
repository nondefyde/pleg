<?php

namespace App\Models\MasterRecords;

use Illuminate\Database\Eloquent\Model;

class PledgeSubMenuItem extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pledge_sub_menu_items';

    /**
     * The table Ranks primary key
     *
     * @var int
     */
    protected $primaryKey = 'pledge_sub_menu_item_id';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['pledge_sub_menu_item', 'pledge_sub_menu_item_url', 'pledge_sub_menu_item_icon', 'active', 'sequence', 'menu_item_id'];

    /**
     * A Menu Item belongs to a Menu
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pledgeMenuItem(){
        return $this->belongsTo('App\Models\MasterRecords\PledgeMenuItem');
    }

    /**
     * Get the roles associated with the given sub menu item
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pledgeRoles()
    {
        return $this->belongsToMany('App\Models\RolesAndPermissions\Role', 'pledge_roles_sub_menu_items', 'pledge_sub_menu_item_id', 'role_id');
    }
}
