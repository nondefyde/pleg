<?php

namespace App\Models\Basic;

use Illuminate\Database\Eloquent\Model;

class Salutation extends Model
{
    /**
     * The table constituencies primary key
     *
     * @var int
     */
    protected $primaryKey = 'salutation_id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'salutations';

    /**
     * disable the time stamps
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['salutation', 'salutation_abbr'];
}
