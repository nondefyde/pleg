<?php

namespace App\Models\RolesAndPermissions;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    /**
     * The table permissions primary key
     * @var int
     */
    protected $primaryKey = 'permission_id';

    /**
     * A Permission belongs to 1 or many Roles
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\RolesAndPermissions\Role');
    }

}
