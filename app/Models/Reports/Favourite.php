<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'favourites';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'favourite_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['mobile_user_id', 'report_id'];

    /**
     * This will get the mobile user that favorite the report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mobileUser(){
        return $this->belongsTo('App\Models\MobileUser');
    }

    /**
     * This will get the report that was favorite
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report(){
        return $this->belongsTo('App\Models\Reports\Report');
    }

}
