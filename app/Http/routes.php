<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Dependent List Box

Route::group(array('prefix'=>'list-box'), function(){
    // Ajax Get Local Governments Based on the state
    Route::get('/lga/{id}', 'ListBoxController@lga');
});

//Route::get('/', 'DashboardController@getIndex');
//Route::get('/home', 'DashboardController@getIndex');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'DashboardController@getIndex');
    Route::get('/home', 'DashboardController@getIndex');

    Route::controller('profiles', 'Users\ProfilesController');
    Route::controller('reports', 'Reports\ReportController');
    Route::controller('mobile-users', 'Users\MobileUsersController');

    Route::group(['middleware' => ['role:developer|admin']], function() {
//        Route::controller('users', 'Users\UserController');
        Route::controller('pledge-menus', 'MasterRecords\PledgeMenusController');
        Route::controller('pledge-menu-items', 'MasterRecords\PledgeMenuItemsController');
        Route::controller('pledge-sub-menu-items', 'MasterRecords\PledgeSubMenuItemsController');
        Route::controller('report-roles', 'Reports\ReportRolesController');

        Route::get('/users', 'Users\UserController@getIndex');
        Route::get('/users/index', 'Users\UserController@getIndex');
        Route::get('/users/create', 'Users\UserController@getCreate');
        Route::post('/users/create', 'Users\UserController@postCreate');
        Route::get('/users/status/{user_id}/{status}', 'Users\UserController@getStatus');
        Route::get('/users/show/{encodeId}', 'Users\UserController@getShow');
    });

    Route::group(['middleware' => ['role:admin']], function() {
        Route::get('/users/edit/{encodeId}', 'Users\UserController@getEdit');
        Route::post('/users/edit/{encodeId}', 'Users\UserController@postEdit');

        Route::controller('sectors', 'MasterRecords\SectorController');
        Route::controller('user-types', 'MasterRecords\UserTypeController');
        Route::controller('roles', 'MasterRecords\RolesController');
        Route::controller('permissions', 'MasterRecords\PermissionsController');

        Route::controller('menus', 'MasterRecords\MenusController');
        Route::controller('menu-items', 'MasterRecords\MenuItemsController');
        Route::controller('sub-menu-items', 'MasterRecords\SubMenuItemController');
    });

    Route::get('/users/change', 'Users\UserController@getChange');
    Route::post('/users/change', 'Users\UserController@postChange');
    Route::post('/users/upload-picture', 'Users\UserController@postUploadPicture');
    Route::post('/users/crop-picture', 'Users\UserController@postCropPicture');

});

Route::controller('auth', 'Auth\AuthController');

Route::controllers([

//    'auth' => 'Auth\AuthController',
//    'users' => 'Users\UserController',
//    'mobile-users' => 'Users\MobileUsersController',
//    'profiles' => 'Users\ProfilesController',

//    'reports' => 'Reports\ReportController',
//    'report-roles' => 'Reports\ReportRolesController',

    //MasterRecords
//    'menus' => 'MasterRecords\MenusController',
//    'menu-items' => 'MasterRecords\MenuItemsController',
//    'sub-menu-items' => 'MasterRecords\SubMenuItemController',
//    'roles' => 'MasterRecords\RolesController',
//    'permissions' => 'MasterRecords\PermissionsController',

//    'pledge-menus' => 'MasterRecords\PledgeMenusController',
//    'pledge-menu-items' => 'MasterRecords\PledgeMenuItemsController',
//    'pledge-sub-menu-items' => 'MasterRecords\PledgeSubMenuItemsController',

//    'sectors' => 'MasterRecords\SectorController',
//    'user-types' => 'MasterRecords\UserTypeController',
]);