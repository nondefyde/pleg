<?php

namespace App\Http\Controllers\MasterRecords;

use App\Models\MasterRecords\UserType;
use App\Models\RolesAndPermissions\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    /**
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display a listing of the Roles for Master Records.
     *
     * @return Response
     */
    public function getIndex()
    {
        $user_type_list = UserType::orderBy('user_type')->lists('user_type', 'user_type')->put('', 'Select Role');
        $user_types = UserType::orderBy('user_type')->lists('user_type', 'user_type_id')->put('', 'Select User Type');
        $roles = Role::orderBy('name')->get();

        return view('records.roles.index', compact('roles', 'user_type_list', 'user_types'));
    }

    /**
     * Insert or Update the menu items records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['role_id']); $i++){
            $role = ($inputs['role_id'][$i] > 0) ? Role::find($inputs['role_id'][$i]) : new Role();
            $role->name = $inputs['name'][$i];
            $role->display_name = $inputs['display_name'][$i];
            $role->description = $inputs['description'][$i];
            $role->user_type_id = $inputs['user_type_id'][$i];
            $count = ($role->save()) ? $count+1 : '';
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Roles has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/roles');
    }

    /**
     * Display a listing of the Users and Roles for Master Records.
     * @param $encodeId
     * @return Response
     */
    public function getUsersRoles($encodeId=null){
        $decodeId = ($encodeId === null) ?  Role::DEFAULT_ROLE : $this->getHashIds()->decode($encodeId)[0];
        $role = Role::findorFail($decodeId);

        $roles = Role::orderBy('display_name')->lists('display_name','role_id');

        $users = $role->users()->get();

        return view('records.roles.users', compact('users', 'roles', 'role', 'encodeId'));
    }

    /**
     * Display a listing of the Permissions for Master Records.
     * @param Request $request
     * @return Response
     */
    public function postUsersRoles(Request $request){
        $inputs = $request->all();
        for($i = 0; $i < count($inputs['user_id']); $i++){
            $user = ($inputs['user_id'][$i] > 0) ? User::find($inputs['user_id'][$i]) : null;

            (isset($inputs['role_id']['role'.$inputs['user_id'][$i]]))
                ? $user->roles()->sync($inputs['role_id']['role'.$inputs['user_id'][$i]]) : $user->roles()->sync([]);
        }

        // Set the flash message
        $this->setFlashMessage(' Permissions has been successfully added to the role.', 1);

        return redirect($request->fullUrl());
    }

    /**
     * Get The Users Given a role id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postRoles(Request $request)
    {
        $inputs = $request->all();
        return redirect('/roles/users-roles/' . $this->getHashIds()->encode($inputs['role_id']));
    }
}
