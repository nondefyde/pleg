<?php

namespace App\Http\Controllers\MasterRecords;

use App\Models\MasterRecords\PledgeMenuItem;
use App\Models\MasterRecords\PledgeSubMenuItem;
use App\Models\MasterRecords\UserType;
use App\Models\RolesAndPermissions\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PledgeSubMenuItemsController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display a listing of the Menu Items for Master Records.
     * @param String $encodeId
     * @return Response
     */
    public function getIndex($encodeId=null)
    {
        $pledge_menu_item_id = '';
        if($encodeId === null) {
            $pledge_sub_menu_items = PledgeSubMenuItem::orderBy('pledge_menu_item_id', 'sequence')->get();
        }else{
            $pledge_menu_item_id = $this->getHashIds()->decode($encodeId)[0];
            $pledge_sub_menu_items = PledgeSubMenuItem::where('pledge_menu_item_id', $pledge_menu_item_id)->orderBy('pledge_menu_item_id', 'sequence')->get();
        }

        $pledge_menu_item_lists = PledgeMenuItem::orderBy('pledge_menu_item')->lists('pledge_menu_item', 'pledge_menu_item_id')->put('', 'Select Menu Item');
        $roles = Role::whereIn('user_type_id', (new UserType())->PLEDGE_GOVT)->orderBy('name')->get();

        return view('records.sub-menu-items.pledge', compact('pledge_sub_menu_items', 'pledge_menu_item_lists', 'roles', 'pledge_menu_item_id'));
    }

    /**
     * Insert or Update the menu items records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for ($i = 0; $i < count($inputs['pledge_sub_menu_item_id']); $i++) {
            $sub_menu_item = ($inputs['pledge_sub_menu_item_id'][$i] > 0) ? PledgeSubMenuItem::find($inputs['pledge_sub_menu_item_id'][$i]) : new PledgeSubMenuItem();
            $sub_menu_item->pledge_sub_menu_item = $inputs['pledge_sub_menu_item'][$i];
            $sub_menu_item->pledge_sub_menu_item_url = $inputs['pledge_sub_menu_item_url'][$i];
            $sub_menu_item->pledge_sub_menu_item_icon = $inputs['pledge_sub_menu_item_icon'][$i];
            $sub_menu_item->active = $inputs['active'][$i];
            $sub_menu_item->sequence = $inputs['sequence'][$i];
            $sub_menu_item->pledge_menu_item_id = $inputs['pledge_menu_item_id'][$i];
//            $count = ($sub_menu_item->save()) ? $count + 1 : '';

            if($sub_menu_item->save()){
                $count = $count+1;
                (isset($inputs['role_id'][$inputs['pledge_sub_menu_item'][$i]]))
                    ? $sub_menu_item->pledgeRoles()->sync($inputs['role_id'][$inputs['pledge_sub_menu_item'][$i]]) : $sub_menu_item->pledgeRoles()->sync([]);
            }
        }
        // Set the flash message
        if ($count > 0)
            $this->setFlashMessage($count . ' Sub Menu Items has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/pledge-sub-menu-items');
    }

    /**
     * Delete a Menu from the list of Menus using a given menu id
     * @param $id
     */
    public function getDelete($id)
    {
        $pledge_sub_menu_item = PledgeSubMenuItem::findOrFail($id);
        //Delete The Warder Record
        $delete = ($pledge_sub_menu_item !== null) ? $pledge_sub_menu_item->delete() : null;

        if ($delete) {
            //Delete its Equivalent Users Record
            $this->setFlashMessage('  Deleted!!! ' . $pledge_sub_menu_item->pledge_sub_menu_item . ' pledge sub menu item have been deleted.', 1);
        } else {
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }

    /**
     * Get The Sub Menu Items Given a menu item id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postMenuItem(Request $request)
    {
        $inputs = $request->all();
        return redirect('/pledge-sub-menu-items/index/' . $this->getHashIds()->encode($inputs['pledge_menu_item_id']));
    }
}
