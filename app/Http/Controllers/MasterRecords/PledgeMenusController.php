<?php

namespace App\Http\Controllers\MasterRecords;

use App\Models\MasterRecords\PledgeMenu;
use App\Models\MasterRecords\UserType;
use App\Models\RolesAndPermissions\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PledgeMenusController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display a listing of the Menus for Master Records.
     *
     * @return Response
     */
    public function getIndex()
    {
        $pledgeMenus = PledgeMenu::orderBy('sequence')->get();
        $roles = Role::whereIn('user_type_id', (new UserType())->PLEDGE_GOVT)->orderBy('name')->get();
        return view('records.menus.pledge', compact('pledgeMenus', 'roles'));
    }

    /**
     * Insert or Update the menu records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['pledge_menu_id']); $i++){
            $pledge_menu = ($inputs['pledge_menu_id'][$i] > 0) ? PledgeMenu::find($inputs['pledge_menu_id'][$i]) : new PledgeMenu();
            $pledge_menu->pledge_menu = $inputs['pledge_menu'][$i];
            $pledge_menu->pledge_menu_url = $inputs['pledge_menu_url'][$i];
            $pledge_menu->pledge_icon = $inputs['pledge_icon'][$i];
            $pledge_menu->active = $inputs['active'][$i];
            $pledge_menu->sequence = $inputs['sequence'][$i];
//            $count = ($menu->save()) ? $count+1 : '';
            if($pledge_menu->save()){
                $count = $count+1;
                (isset($inputs['role_id'][$inputs['pledge_menu'][$i]]))
                    ? $pledge_menu->pledgeRoles()->sync($inputs['role_id'][$inputs['pledge_menu'][$i]]) : $pledge_menu->pledgeRoles()->sync([]);
            }
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Menus has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/pledge-menus');
    }

    /**
     * Delete a Menu from the list of Menus using a given menu id
     * @param $id
     */
    public function getDelete($id)
    {
        $pledge_menu = PledgeMenu::findOrFail($id);
        //Delete The Warder Record
        $delete = ($pledge_menu !== null) ? $pledge_menu->delete() : null;

        if($delete){
            //Delete its Equivalent Users Record
            $this->setFlashMessage('  Deleted!!! '.$pledge_menu->pledge_menu.' menu have been deleted.', 1);
        }else{
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }
}
