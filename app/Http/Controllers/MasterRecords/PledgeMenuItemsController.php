<?php

namespace App\Http\Controllers\MasterRecords;

use App\Models\MasterRecords\UserType;
use App\Models\RolesAndPermissions\Role;
use Illuminate\Http\Request;
use App\Models\MasterRecords\PledgeMenu;
use App\Models\MasterRecords\PledgeMenuItem;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PledgeMenuItemsController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display a listing of the Menu Items for Master Records.
     * @param String $encodeId
     * @return Response
     */
    public function getIndex($encodeId=null)
    {
        $pledge_menu_id = '';
        if($encodeId === null) {
            $pledge_menu_items = PledgeMenuItem::orderBy('pledge_menu_id', 'sequence')->get();
        }else{
            $pledge_menu_id = $this->getHashIds()->decode($encodeId)[0];
            $pledge_menu_items = PledgeMenuItem::where('pledge_menu_id', $pledge_menu_id)->orderBy('pledge_menu_id', 'sequence')->get();
        }

        $pledge_menu_lists = PledgeMenu::orderBy('pledge_menu')->lists('pledge_menu', 'pledge_menu_id')->put('', 'Select Menu');
        $roles = Role::whereIn('user_type_id', (new UserType())->PLEDGE_GOVT)->orderBy('name')->get();

        return view('records.menu-items.pledge', compact('pledge_menu_items', 'pledge_menu_lists', 'roles', 'pledge_menu_id'));
    }

    /**
     * Insert or Update the menu items records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['pledge_menu_item_id']); $i++){
            $menu_item = ($inputs['pledge_menu_item_id'][$i] > 0) ? PledgeMenuItem::find($inputs['pledge_menu_item_id'][$i]) : new PledgeMenuItem();
            $menu_item->pledge_menu_item = $inputs['pledge_menu_item'][$i];
            $menu_item->pledge_menu_item_url = $inputs['pledge_menu_item_url'][$i];
            $menu_item->pledge_menu_item_icon = $inputs['pledge_menu_item_icon'][$i];
            $menu_item->active = $inputs['active'][$i];
            $menu_item->sequence = $inputs['sequence'][$i];
            $menu_item->pledge_menu_id = $inputs['pledge_menu_id'][$i];
//            $count = ($menu_item->save()) ? $count+1 : '';

            if($menu_item->save()){
                $count = $count+1;
                (isset($inputs['role_id'][$inputs['pledge_menu_item'][$i]]))
                    ? $menu_item->pledgeRoles()->sync($inputs['role_id'][$inputs['pledge_menu_item'][$i]]) : $menu_item->pledgeRoles()->sync([]);
            }
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Pledge Menu Items has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/pledge-menu-items');
    }

    /**
     * Delete a Menu from the list of Menus using a given menu id
     * @param $id
     */
    public function getDelete($id)
    {
        $pledge_menu_item = PledgeMenuItem::findOrFail($id);
        //Delete The Warder Record
        $delete = ($pledge_menu_item !== null) ? $pledge_menu_item->delete() : null;
        if($delete){
            //Delete its Equivalent Users Record
            $this->setFlashMessage('  Deleted!!! '.$pledge_menu_item->pledge_menu_item.' menu item have been deleted.', 1);
        }else{
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }

    /**
     * Get The Menu Items Given a menu id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postMenu(Request $request)
    {
        $inputs = $request->all();
        return redirect('/pledge-menu-items/index/' . $this->getHashIds()->encode($inputs['pledge_menu_id']));
    }
}
