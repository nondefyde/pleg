<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Models\MasterRecords\UserType;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Basic\State;
use App\Models\MasterRecords\Sector;
use App\User;

class ReportRolesController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display a listing of the Users.
     * @return Response
     */
    public function getIndex()
    {
        $users = User::where('user_type_id', '<=', UserType::USER)->orderBy('first_name')->get();
        $sectors = Sector::orderBy('sector')->get();
        $states = State::orderBy('state')->get();

        return view('reports.roles.users', compact('users', 'sectors', 'states'));
    }

    /**
     * Manage users reports access levels
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['user_id']); $i++){
            $user = User::find($inputs['user_id'][$i]);

            if($user){
                $count++;
                (isset($inputs['sector_id'][$user->email]))
                    ? $user->reportSectors()->sync($inputs['sector_id'][$user->email])
                    : $user->reportSectors()->sync([]);

                (isset($inputs['state_id'][$user->email]))
                    ? $user->reportStates()->sync($inputs['state_id'][$user->email])
//                    ? $user->reportStates()->sync($inputs['state_id'][$user->email], ['category_id' => $inputs['category_id'][$i]])
                    : $user->reportStates()->sync([]);
            }
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Users has been successfully given access.', 1);

        return redirect('/report-roles');
    }

    /**
     * Display a listing of the Users.
     * @return Response
     */
    public function getExecutives()
    {
        $users = User::where('user_type_id', UserType::EXECUTIVES)->orderBy('first_name')->get();
        $states = State::orderBy('state')->get();

        return view('reports.roles.executives', compact('users', 'states'));
    }

    /**
     * Manage users reports access levels
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postExecutives(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['user_id']); $i++){
            $user = User::find($inputs['user_id'][$i]);

            if($user){
                $count++;
                (isset($inputs['state_id'][$user->email]))
                    ? $user->reportStates()->sync($inputs['state_id'][$user->email])
                    : $user->reportStates()->sync([]);
            }
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Users has been successfully given access.', 1);

        return redirect('/report-roles/executives');
    }

    /**
     * Display a listing of the Users.
     * @return Response
     */
    public function getLegislators()
    {
        $users = User::where('user_type_id', UserType::LEGISLATORS)->orderBy('first_name')->get();
        $states = State::orderBy('state')->get();

        return view('reports.roles.legislators', compact('users', 'states'));
    }

    /**
     * Manage users reports access levels
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLegislators(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['user_id']); $i++){
            $user = User::find($inputs['user_id'][$i]);

            if($user){
                $count++;
                (isset($inputs['state_id'][$user->email]))
                    ? $user->reportStates()->sync($inputs['state_id'][$user->email])
                    : $user->reportStates()->sync([]);
            }
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Users has been successfully given access.', 1);

        return redirect('/report-roles/legislators');
    }

    /**
     * Display a listing of the Users.
     * @return Response
     */
    public function getSectors()
    {
        $users = User::where('user_type_id', UserType::SECTORS)->orderBy('first_name')->get();
        $sectors = Sector::orderBy('sector')->get();
        $states = State::orderBy('state')->get();

        return view('reports.roles.sectors', compact('users', 'sectors', 'states'));
    }

    /**
     * Manage Sectors users reports access levels
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postSectors(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['user_id']); $i++){
            $user = User::find($inputs['user_id'][$i]);

            if($user){
                $count++;
                (isset($inputs['sector_id'][$user->email]))
                    ? $user->reportSectors()->sync($inputs['sector_id'][$user->email])
                    : $user->reportSectors()->sync([]);

                (isset($inputs['state_id'][$user->email]))
                    ? $user->reportStates()->sync($inputs['state_id'][$user->email])
//                    ? $user->reportStates()->sync($inputs['state_id'][$user->email], ['category_id' => $inputs['category_id'][$i]])
                    : $user->reportStates()->sync([]);
            }
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Users has been successfully given access.', 1);

        return redirect('/report-roles/sectors');
    }
}