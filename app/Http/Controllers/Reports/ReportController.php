<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\MobileUser;
use App\Models\Reports\GovernmentReport;
use App\Models\Reports\ReportImage;
use Illuminate\Http\Request;
use App\Models\Basic\State;
use App\Models\MasterRecords\Sector;
use App\Models\Reports\Report;
use Illuminate\Support\Facades\Auth;
use Validator;

class ReportController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Get a validator for an incoming registration request.
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'title.required' => 'The Report Title is Required!',
            'sector_id.required' => 'The Report Sector is Required!',
            'mobile_user_id.required' => 'The Reported By is Required!',
            'state_id.required' => 'The Report State is Required!',
        ];
        return Validator::make($data, [
            'title' => 'required',
            'sector_id' => 'required',
            'mobile_user_id' => 'required',
            'state_id' => 'required',
        ], $messages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $user = Auth::user();
        $reports = Report::whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray())
            ->where('forward', 0)->where('archive', 0)
            ->where(function ($query) use ($user) {
                ($user->reportStates()->count() > 0)
                    ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray())
                    : '';
            })
            ->orderBy('created_at')->get();

        return view('reports.index', compact('reports', 'user'));
    }

    /**
     * Display a listing of the resource.
     * @param String $encodeId
     * @param String $read
     * @return \Illuminate\Http\Response
     */
    public function getShow($encodeId)
    {
        $user = Auth::user();
        $decodeId = $this->getHashIds()->decode($encodeId);
        $report = (empty($decodeId)) ? abort(305) : Report::findOrFail($decodeId[0]);

        if($report){ $report->notification = 1; $report->save(); }

        return view('reports.show', compact('report', 'user'));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getForwarded()
    {
        $user = Auth::user();
        $reports = Report::where('forward', 1)->whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray())
            ->where(function ($query) use ($user) {
                ($user->reportStates()->count() > 0)
                    ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray())
                    : '';
            })
            ->orderBy('updated_at', 'desc')->get();

        return view('reports.forwarded', compact('reports', 'user'));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getArchived()
    {
        $user = Auth::user();
        $reports = Report::where('archive', 1)->whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray())
            ->where(function ($query) use ($user) {
                ($user->reportStates()->count() > 0)
                    ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray())
                    : '';
            })
            ->orderBy('updated_at', 'desc')->get();

        return view('reports.archived', compact('reports', 'user'));
    }

    /**
     * Get all approved reports
     * @return Response
     */
    public function getApproved()
    {
        $user = Auth::user();
        $reports = Report::whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray())
            ->where(function ($query) {
                $query->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::APPROVED)
                    ->lists('report_id')->toArray());
            })
            ->where(function ($query) use ($user) {
                ($user->reportStates()->count() > 0)
                    ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray())
                    : '';
            })
            ->orderBy('updated_at', 'desc')->get();

        return view('reports.approved', compact('reports', 'user'));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getCreate()
    {
        $sectors = Sector::orderBy('sector')->lists('sector', 'sector_id')->put('', 'Nothing Selected');
        $states = State::orderBy('state')->lists('state', 'state_id')->put('', 'Nothing Selected');
        $mobile_users = MobileUser::orderBy('first_name')->get();

        return view('reports.create', compact('sectors', 'mobile_users', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request  $request
     * @return Response
     */
    public function postCreate(Request $request)
    {
        $inputs = $request->all();
        //Validate Request Inputs
        if ($this->validator($inputs)->fails())
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('reports/create')->withErrors($this->validator($inputs))->withInput();
        }

        // Store the Report...
        $report = Report::create($inputs);

        if($report) {
            if($request->hasFile('image_url'))
            {
                $file = $report->report_id . '_report.' . $request->file('image_url')->getClientOriginalExtension();
                $request->file('image_url')->move(base_path() . '/public/uploads/reports/', $file);

                //Update File Path
                $report_image = new ReportImage();
                $report_image->report_id = $report->report_id;
                $report_image->image_url = $file;
                $report_image->save();
            }
            // Set the flash message
            $this->setFlashMessage('Saved!!! ' . $report->title . ' have successfully been saved', 1);
            // redirect to the create new warder page
            return redirect('reports/create');
        }
    }

    /**
     * Display a summary of the report
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getDetail($id)
    {
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        $report->images = $report->images();
        $report->hashedId = $this->getHashIds()->encode($report->report_id);
        $report->path = $report->file_path;
        $report->user = $report->mobileUser()->first();
        $report->state = $report->state()->first()->state;
        $report->user_hashed = $this->getHashIds()->encode($report->mobileUser()->first()->mobile_user_id);
        $report->avatar = ($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg';

        return response()->json($report);
    }

    /**
     * Send a Report
     * @param  int  $id
     * @return Response
     */
    public function getForward($id)
    {
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        if($report) {
            $report->forward = 1;
            $report->archive = 0;
            //update The Report
            if($report->save()){
                //Insert a copy into the government report table
                $govt = new GovernmentReport();
                $govt->report_id = $report->report_id;
                $govt->save();
                $this->setFlashMessage(' Forwarded!!! '.$report->title.' Report have been sent.', 1);
            }else{
                $this->setFlashMessage('Error!!! Unable to perform task try again.', 2);
            }

        }
    }

    /**
     * Archive the report by disabling the status = 0
     * @param  int  $id
     * @return Response
     */
    public function getArchive($id, $val)
    {
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        //Remove The Report Record
        if($report !== null) {
            $report->archive = $val;
            $report->forward = 0;
            //Update The Report for Archiving
            ($report->save())
                ? $this->setFlashMessage(' Updated!!! '.$report->title.' Report have been updated.', 1)
                : $this->setFlashMessage('Error!!! Unable to Archive record.', 2);
        }
    }

    /**
     * Get the newly created report so as to trigger a notification
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getNewReport($id)
    {
        $user = Auth::user();
        $report = (empty($id)) ? abort(305) : Report::find($id);
//            : Report::where('report_id', $id)->whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray())
//                ->where(function ($query) use ($user) {
//                    ($user->reportStates()->count() > 0)
//                        ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray())
//                        : '';
//                });
        if($report) {
            $report->hashId = $this->getHashIds()->encode($report->report_id);
            $report->date = ($report->created_at) ? $report->created_at->format('D, jS, M Y') : '';
            $report->ago = ($report->created_at) ? $report->created_at->diffForHumans() : '';
            $report->names = $report->mobileUser()->first()->fullNames();
            $report->avatar = ($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg';
            return response()->json($report);
        }
    }

    /**
     * Get all the reports that are un read where notification = 0
     * @return Response
     */
    /*public function getUnread()
    {
        $user = Auth::user();
        $reports = Report::where('notification', 0)->where('forward', 0)->where('archive', 0)
            ->whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray())
            ->where(function ($query) use ($user) {
                ($user->reportStates()->count() > 0)
                    ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray())
                    : '';
            })
            ->orderBy('created_at', 'desc')->get(['title', 'report_id', 'mobile_user_id', 'created_at']);
        foreach($reports as $report){
            $report->hashId = $this->getHashIds()->encode($report->report_id);
            $report->date = ($report->created_at) ? $report->created_at->format('D, jS, M Y') : '';
            $report->ago = ($report->created_at) ? $report->created_at->diffForHumans() : '';
            $report->names = $report->mobileUser()->first()->fullNames();
            $report->avatar = ($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg';
        }

        return response()->json($reports);
    }*/

    /**
     * Get all the reports that are from the sector
     * @param String $encodeId
     * @return Response
     */
    public function getSectors($encodeId)
    {
        $user = Auth::user();
        $decodeId = $this->getHashIds()->decode($encodeId);
        $sector = (empty($decodeId)) ? abort(305) : Sector::findOrFail($decodeId[0]);
        $reports = $user->getReportSectors($sector->sector_id);

        return view('reports.sectors', compact('reports', 'user', 'sector'));
    }

    /**
     * Get all the reports that are from the state
     * @param String $encodeId
     * @return Response
     */
    public function getStates($encodeId)
    {
        $user = Auth::user();
        $decodeId = $this->getHashIds()->decode($encodeId);
        $state = (empty($decodeId)) ? abort(305) : State::findOrFail($decodeId[0]);
        $reports = $user->getReportStates($state->state_id);

        return view('reports.states', compact('reports', 'user', 'state'));
    }
}