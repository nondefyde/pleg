<?php

namespace App\Http\Controllers;

use App\Models\Reports\Report;
use Hashids\Hashids;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

//    public $authUser = null;

    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $user = Auth::user();
        if($user) {
//            $this->authUser = $user;
            $reports = $this->getUnreadReports($user);
            View::share('report_notifications', $reports);

            //Check if the user has permission to perform such action
//            $this->checkPermission();
        }
    }

    /**
     * Set The HashIds Secret Key, Length and Possible Characters Combinations
     * @return Hashids
     */
    public function getHashIds()
    {
        return new Hashids(env('APP_KEY'), 15, env('APP_CHAR'));
    }

    /**
     * @param  string  $msg
     * @param int $type
     * @return void
     */
    public function setFlashMessage($msg, $type)
    {
        $class1 = 'alert-info';
        $class2 = 'fa fa-info fa-2x';

        if($type == 1){
            $class1 = 'alert-success';
            $class2 = 'fa fa-thumbs-o-up fa-2x';
        }elseif($type == 2){
            $class1 = 'alert-danger';
            $class2 = 'fa fa-thumbs-o-down fa-2x';
        }

        $output =   '<div class="alert '.$class1.'" id="flash_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <i class="'.$class2.'"></i> <strong>' . $msg . '</strong>'.
            '</div>';
        \Session::flash('flash_message', $output);
    }

    /**
     * Get all the reports that are un read where read = 0
     * @param User $user
     * @return Response
     */
    private function getUnreadReports($user)
    {
        $reports = Report::where('notification', 0)->where('forward', 0)->where('archive', 0)
            ->whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray())
            ->where(function ($query) use ($user) {
                ($user->reportStates()->count() > 0)
                    ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray())
                    : '';
            })
            ->orderBy('created_at', 'desc');
//            ->orderBy('created_at', 'desc')->get(['title', 'report_id', 'mobile_user_id', 'created_at']);
        return $reports;
    }

    /**
     * Check if the user has permission to perform such action
     * @return Response
     */
    protected function checkPermission(){
        $action = Route::currentRouteAction();
        $permission = substr($action, strripos($action, '\\') + 1);
        $method = explode('@', $permission)[1];
        if(substr($method, 0, 4) !== 'post' && !Auth::user()->canPerform($permission)){
//        dd(Auth::user()->canPerform($permission));
            abort(403);
        }
    }

    /**
     * Get an array containing the names of all controllers in the application
     * @return Array
     */
//    private function getControllers(){
//        $controllers = [];
//        foreach (Route::getRoutes()->getRoutes() as $route)
//        {
//            $action = $route->getAction();
//
//            if (array_key_exists('controller', $action))
//            {
//                // You can also use explode('@', $action['controller']); here
//                // to separate the class name from the method
//                $slash = strripos($action['controller'], '\\');
//                $at = strripos($action['controller'], '@');
//                $controllers[] = substr($action['controller'], $slash + 1, ($at - $slash - 1));
//            }
//        }
//        $controllers = array_unique($controllers);
//        sort($controllers);
//
//        return $controllers;
//    }
}
