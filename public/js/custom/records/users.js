/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {

    //Button to Activate or Deactivate a User
    $(document.body).on('click', '.user_status',function(e){
        var box = $("#confirm-status-row");
        var parent = $(this).parent().parent();
        var type = ($(this).attr('rel') === '1') ? 'Activate' : 'Deactivate';

        $("#status_name").text(type + ' ' + parent.children(':nth-child(2)').text() + ' ' + parent.children(':nth-child(3)').text());
        $("#confirm_status").val($(this).val());
        $("#confirm_status").attr('rel', $(this).attr('rel'));
        box.addClass("open");
    });

    //Activating or Deactivating a User
    $(document.body).on('click', '#confirm_status',function(e){
        //e.preventDefault();
        var box = $("#confirm-status-row");
        var user_id = $(this).val();
        var status_id = $(this).attr('rel');
        $.ajax({
            type: 'GET',
            async: true,
            url: '/users/status/' + user_id + '/' + status_id,
            success: function(data,textStatus){
                box.removeClass("open");
                location.reload();
            },
            error: function(xhr,textStatus,error){
                alert(textStatus + ' ' + xhr);
            }
        });
        return false;
    });


    function onSuccess(){
        $("#cp_photo").parent("a").find("span").html("Choose another photo");

        var img = $("#cp_target").find("#crop_image")

        if(img.length === 1){
            $("#cp_img_path").val(img.attr("src"));

            img.cropper({aspectRatio: 1,
                done: function(data) {
                    $("#ic_x").val(data.x);
                    $("#ic_y").val(data.y);
                    $("#ic_h").val(data.height);
                    $("#ic_w").val(data.width);
                }
            });

            $("#cp_accept").prop("disabled",false).removeClass("disabled");

            $("#cp_accept").on("click",function(){

                $("#user_image").html("<img src='/img/loaders/default.gif'/>");
                $("#modal_change_photo").modal("hide");

                $.ajax({
                    url: '/users/crop-picture',
                    data: $('#cp_crop').serialize(),
                    method :'POST',
                    success: function(data){
                        $("#user_image").html('<img src="/' +data +'" class="img-thumbnail"/>');
                        //$(".p_image").html('<img src="/' +data +'"/>');
                        console.log(data);
                    },
                    error: function(error){
                        console.log(error);
                    }
                });

                $("#cp_target").html("Use form below to upload file. Only .jpg files.");
                $("#cp_photo").val("").parent("a").find("span").html("Select file");
                $("#cp_accept").prop("disabled",true).addClass("disabled");
                $("#cp_img_path").val("");
            });
        }
    }

    $("#cp_photo").on("change",function(){

        if($("#cp_photo").val() == '') return false;

        $("#cp_target").html('<img src="/img/loaders/default.gif"/>');
        $("#cp_upload").ajaxForm({target: '#cp_target',success: onSuccess}).submit();

    });

    //Change Password via modal in users profile
    $(document.body).on('submit', '#password_change_form',function(e){
        //e.preventDefault();
        var values = $(this).serialize();
        $.ajax({
            type: 'POST',
            async: true,
            data: values,
            url: '/users/change',
            success: function(data,textStatus){
                if(data.status == 0){
                    $('#error_msg').html(data.msg);
                    $('#error_msg').removeClass('hide');
                    $('#error_msg').addClass('alert-danger');
                }else if(data.status == 1){
                    $('#msg_box').removeClass('hide');
                    $('#msg_box').addClass('alert-success');
                    $('#msg_box').html(data.msg);
                    $('#modal_change_password').modal('hide');
                    $('#msg_box').delay(8000).slideUp(850);
                }
                //location.reload();
            },
            error: function(xhr,textStatus,error){
                alert(textStatus + ' ' + xhr);
            }
        });
        return false;
    });

    //Restrict user from selecting future dates as date of birth
    //$( "#dob" ).datepicker({ dateFormat: "yyyy-mm-dd",  maxDate: -1 });


    // Ajax Get Local Governments Based on the state
    getDependentListBox($('#state_id'), $('#lga_id'), '/list-box/lga/');
});